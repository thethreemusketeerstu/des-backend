Feature: Integration tests for client endpoints
  Background:
    Given there exists a user
    And the user is logged in

  Scenario: Get profile
    When a request for getting the user's profile is sent
    Then the server response is 'OK'
    And the user's profile is returned

  Scenario: Update profile
    When a request for updating the user's password is sent
    Then the server response is 'OK'
    And the user's password is updated

  Scenario: Get devices
    And there exist several devices associated to the user
    When a request for getting all devices is sent
    Then the server response is 'OK'
    And a list of all devices is returned

  Scenario: Create a device
    When a request for creating a device is sent
    Then the server response is 'OK'
    And the new device is returned
    And the new device is persisted

  Scenario: Get a device
    And there exists a device associated to the user 
    When a request for getting the device is sent
    Then the server response is 'OK'
    And the expected device is returned

  Scenario: Update a device
    And there exists a device associated to the user 
    When a request for updating the device is sent
    Then the server response is 'OK'
    And the updated device is returned
    And the updated device is persisted

  Scenario: Delete a device
    And there exists a device associated to the user 
    When a request for deleting the device is sent
    Then the server response is 'NO_CONTENT'
    And the user is no more an owner of the device

  Scenario: Get average device data
    And there exists a device associated to the user 
    And there is some logged overage data
    When a request for getting the overage data is sent
    Then the server response is 'OK'
    And  the expected average device data is returned

  Scenario: Get current device data
    And there exists a device associated to the user 
    And there is some logged current data
    When a request for getting the current data is sent
    Then the server response is 'OK'
    And the expected current device data is returned



