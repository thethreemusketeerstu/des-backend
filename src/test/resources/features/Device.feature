Feature: Integration tests for device endpoints

  Scenario: Log current device data
    Given there is some current device data
    When a request for persisting the current data is sent
    Then the server response is 'NO_CONTENT'
    And a new device is created

  Scenario: Log average device data
    Given there is some average device data
    When a request for persisting the average data is sent
    Then the server response is 'NO_CONTENT'
    And a new device is created
    And the average device data is persisted