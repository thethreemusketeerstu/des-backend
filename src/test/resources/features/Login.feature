Feature: User authentication

  Scenario: Authenticate a user
    Given there exists a user
    When a request for authentication the user is sent
    Then the server response is 'OK'
    And the authentication token is received

  Scenario: Refresh a client authentication token
    Given there exists a user
    And the user is logged in
    When a request for refreshing the authentication token is sent
    Then the server response is 'OK'
    And the new authentication token is received
