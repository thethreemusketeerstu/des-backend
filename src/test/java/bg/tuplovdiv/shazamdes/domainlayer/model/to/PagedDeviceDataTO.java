package bg.tuplovdiv.shazamdes.domainlayer.model.to;

import bg.tuplovdiv.shazamdes.domainlayer.model.DeviceData;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Created by Nikolay Ivanov on 2018-05-12
 */
@Getter
@Setter
@NoArgsConstructor
public class PagedDeviceDataTO  extends PagedDataTO<DeviceData> {

    public PagedDeviceDataTO(List<DeviceData> data, PageTO page) {
        super(data, page);
    }

}
