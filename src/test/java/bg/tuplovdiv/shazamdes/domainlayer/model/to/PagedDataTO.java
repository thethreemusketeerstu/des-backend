package bg.tuplovdiv.shazamdes.domainlayer.model.to;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Created by Nikolay Ivanov on 2018-05-12
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PagedDataTO<T> {

    private List<T> data;

    private PageTO page;
}