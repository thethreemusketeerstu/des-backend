package bg.tuplovdiv.shazamdes.domainlayer.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by Nikolay Ivanov on 2018-06-09
 */
@Getter
@AllArgsConstructor
public class UserTO {

    private String id;

    private String email;

    private String password;
}
