package bg.tuplovdiv.shazamdes.domainlayer.model.to;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Nikolay Ivanov on 2018-05-12
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PageTO {

    private Integer totalItems;

    private Integer totalPages;

    private Integer pageSize;

    private Integer currentPage;

}