package bg.tuplovdiv.shazamdes.servicelayer.security.jwt;


import bg.tuplovdiv.shazamdes.domainlayer.model.User;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.io.Serializable;

//We need to rewrite the class in the tests
//in order to define a JsonCreator for deserializing server responses
@Getter
public class JwtAuthenticationResponse implements Serializable {

    private static final long serialVersionUID = 1250166508152483573L;

    private String token;
    private User user;

    @JsonCreator
    public JwtAuthenticationResponse(@JsonProperty("token") String token,
                                     @JsonProperty("user") User user) {
        this.token = token;
        this.user = user;
    }
}