package bg.tuplovdiv.shazamdes.stepdefinition;

import bg.tuplovdiv.shazamdes.domainlayer.model.Device;
import bg.tuplovdiv.shazamdes.domainlayer.model.DeviceData;
import bg.tuplovdiv.shazamdes.servicelayer.repository.DeviceDataRepository;
import bg.tuplovdiv.shazamdes.servicelayer.repository.DeviceRepository;
import bg.tuplovdiv.shazamdes.servicelayer.service.CurrentDeviceDataService;
import bg.tuplovdiv.shazamdes.servicelayer.service.DeviceDataService;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


/**
 * Created by Nikolay Ivanov on 2018-06-10
 */
public class DeviceStepDefinitions extends BaseDefinitions{

    private static final String DEVICE_ID = "1234567890";
    private static final String DEVICE_AUTH_TOKEN = "eyJhbGciOiJIUzUxMiJ9.eyJhdWRpZW5jZSI6ImVtYmVkZGVkIiwiY3JlYXRlZCI6eyJlcG9jaFNlY29uZCI6MTUyNzMzODIxMywibmFubyI6MzYwMDAwMDB9LCJkZXZpY2VJZCI6IjEyMzQ1Njc4OTAifQ.kWAvDtZfd9bnTXfZMC2oFdeN6eio037R-xq5k76J-njMciB5pX8EpVH60gAETOtMCFgoUPLSGBQkJCgvLbigHg";

    @Autowired
    private CurrentDeviceDataService currentDeviceDataService;

    @Autowired
    private DeviceDataService deviceDataService;

    @Autowired
    private DeviceDataRepository deviceDataRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    private DeviceData currentDeviceData;

    private DeviceData averageDeviceData;

    @Given("^there is some current device data$")
    public void thereIsSomeCurrentDeviceData() throws Throwable {
        // Initialize the base class
        init();

        currentDeviceData = createDeviceDataObject();
    }

    private DeviceData createDeviceDataObject() {
         return new DeviceData(
                null,
                null,
                Instant.now(),
                230.1,
                229.1,
                231.1,
                55.5,
                54.5,
                56.5,
                50.0,
                12.6,
                50.1
        );
    }

    @When("^a request for persisting the current data is sent$")
    public void aRequestForPersistingTheCurrentDataIsSent() throws Throwable {
        setAuthenticationToken(DEVICE_AUTH_TOKEN);
        sendAutheticatedPostRequest(getServerAddressAndPort() + "/device/" + DEVICE_ID + "/data/current", currentDeviceData, null);
    }

    @And("^a new device is created$")
    public void aNewDeviceIsCreated() throws Throwable {
        Device device = deviceRepository.findOne(DEVICE_ID);
        assertNotNull(device);
    }

    @Given("^there is some average device data$")
    public void thereIsSomeAverageDeviceData() throws Throwable {
        // Initialize the base class
        init();

        averageDeviceData = createDeviceDataObject();
    }

    @When("^a request for persisting the average data is sent$")
    public void aRequestForPersistingTheAverageDataIsSent() throws Throwable {
        setAuthenticationToken(DEVICE_AUTH_TOKEN);
        sendAutheticatedPostRequest(getServerAddressAndPort() + "/device/" + DEVICE_ID + "/data/average", averageDeviceData, null);

    }

    @And("^the average device data is persisted$")
    public void theAverageDeviceDataIsPersisted() throws Throwable {
        DeviceData persistedData = deviceDataRepository.findFirstByDeviceIdOrderByFromDateDesc(DEVICE_ID);

        assertNotNull("Device data is not persisted", persistedData);

        //Set the id so that we can compare the object with equals()
        averageDeviceData.setId(persistedData.getId());

        assertEquals("The persisted data is not the same as the expected", averageDeviceData, persistedData);
    }
}
