package bg.tuplovdiv.shazamdes.stepdefinition;

import bg.tuplovdiv.shazamdes.StepDefiniton;
import bg.tuplovdiv.shazamdes.domainlayer.model.User;
import bg.tuplovdiv.shazamdes.servicelayer.repository.UserRepository;
import bg.tuplovdiv.shazamdes.servicelayer.security.jwt.CustomUserDetailsService;
import bg.tuplovdiv.shazamdes.servicelayer.security.jwt.JwtTokenUtil;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.Instant;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

;
/**
 * Created by Nikolay Ivanov on 2018-06-09
 */

@StepDefiniton
public class CommonStepDefinitions extends BaseDefinitions{

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private CustomUserDetailsService userDetailsService;

    @Given("^there exists a user$")
    public void thereExistsAUser(){
        init();
        User newUser = new User();
        newUser.setId(UUID.randomUUID().toString());
        newUser.setPasswordHash(passwordEncoder.encode(USER_PASSWORD));
        newUser.setLastPasswordResetDate(Instant.now());
        newUser.setEmail(USER_EMAIL);

        //Store the user in the 'user' variable of the base class
        user = userRepository.save(newUser);

        assertNotNull(user);
    }

    @Then("^the server response is '([A-Z_]+)'$")
    public void theServerResponseIs(String responseValue) {
        HttpStatus expectedHttpStatus = HttpStatus.valueOf(responseValue);

        HttpStatus actualHttpStatus = responseEntity.getStatusCode();

        assertEquals("Status codes do not match", expectedHttpStatus, actualHttpStatus);
    }

    @And("^the user is logged in$")
    public void theUserIsLoggedIn() throws Throwable {
        User userDetails = userDetailsService.loadUserByUsername(user.getEmail());
        String token = jwtTokenUtil.generateToken(userDetails, null);
        setAuthenticationToken(token);
    }
}
