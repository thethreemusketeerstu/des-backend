package bg.tuplovdiv.shazamdes.stepdefinition;

import bg.tuplovdiv.shazamdes.StepDefiniton;
import bg.tuplovdiv.shazamdes.servicelayer.security.jwt.JwtAuthenticationRequest;
import bg.tuplovdiv.shazamdes.servicelayer.security.jwt.JwtAuthenticationResponse;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import org.junit.Assert;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nikolay Ivanov on 2018-06-09
 */

@StepDefiniton
public class AuthenticationStepDefinition extends BaseDefinitions{

    @When("^a request for authentication the user is sent$")
    public void aRequestForAuthenticationTheUserIsSent() {

        JwtAuthenticationRequest authenticationRequest = new JwtAuthenticationRequest(USER_EMAIL, USER_PASSWORD);
        sendPostRequest(getServerAddressAndPort() + "/client/login", authenticationRequest, JwtAuthenticationResponse.class);
    }

    @And("^the authentication token is received$")
    public void theAuthenticationTokenIsReceived() {
        JwtAuthenticationResponse authenticationResponse = getResponseEntityBodyAsType(JwtAuthenticationResponse.class);

        Assert.assertNotNull("No response received", authenticationResponse);
        Assert.assertNotNull("No authentication token received", authenticationResponse.getToken());
    }

    @When("^a request for refreshing the authentication token is sent$")
    public void aRequestForRefreshingTheAuthenticationTokenIsSent() {

        Map<String, String> body = new HashMap<>();
        body.put("token", getAuthenticationToken());

        sendPostRequest(getServerAddressAndPort() + "/client/tokenRefresh", body, JwtAuthenticationResponse.class);
    }

    @And("^the new authentication token is received$")
    public void theNewAuthenticationTokenIsReceived() {
        theAuthenticationTokenIsReceived();
    }
}
