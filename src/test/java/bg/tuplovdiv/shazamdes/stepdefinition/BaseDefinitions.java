package bg.tuplovdiv.shazamdes.stepdefinition;

import bg.tuplovdiv.shazamdes.domainlayer.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

/**
 * Created by Nikolay Ivanov on 2018-06-09
 */
public class BaseDefinitions {

    protected static final String USER_EMAIL = "thethreemusketeers@des.net";
    protected static final String USER_PASSWORD = "123456";
    private static final String SERVER_ADDRES = "http://localhost:";

    protected static ResponseEntity responseEntity;
    protected static User user;

    private static String authenticationToken;

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    protected int port;


    protected void init(){
        user = null;
        responseEntity = null;
        authenticationToken = null;
    }

    public String getServerAddressAndPort() {
        return SERVER_ADDRES + port;
    }

    protected static void setAuthenticationToken(String authenticationToken) {
        BaseDefinitions.authenticationToken = authenticationToken;
    }

    protected static String getAuthenticationToken() {
        return authenticationToken;
    }

    protected void sendPostRequest(String url, Object body, Class<?> responseType){
        this.sendRequest(url, HttpMethod.POST, body, responseType, false);
    }

    protected void sendAutheticatedPostRequest(String url, Object body, Class<?> responseType){
        this.sendRequest(url, HttpMethod.POST, body, responseType, true);
    }

    protected void sendAutheticatedGetRequest(String url, Class<?> responseType){
        this.sendRequest(url, HttpMethod.GET, null, responseType, true);
    }

    protected void sendAutheticatedPutRequest(String url, Object body, Class<?> responseType){
        this.sendRequest(url, HttpMethod.PUT, body, responseType, true);
    }

    protected void sendAutheticatedDeleteRequest(String url){
        this.sendRequest(url, HttpMethod.DELETE, null, null, true);
    }

    private void sendRequest(String url, HttpMethod httpMethod, Object body, Class<?> responseType, boolean useAuthetication){
        HttpHeaders httpHeaders = new HttpHeaders();
        if(useAuthetication){
            if (authenticationToken == null){
                throw new IllegalArgumentException("Cannot execute authenticated request. Access token is not provided.");
            }
            httpHeaders.add("Authorization", authenticationToken);
        }
        httpHeaders.add("Content-Type", "application/json");

        responseEntity = this.restTemplate.exchange(
                url, httpMethod, new HttpEntity<>(body, httpHeaders), responseType
        );
    }

    protected <R> R getResponseEntityBodyAsType(Class<R> classType){
        return classType.cast(responseEntity.getBody());
    }
}
