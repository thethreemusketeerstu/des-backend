package bg.tuplovdiv.shazamdes.stepdefinition;

import bg.tuplovdiv.shazamdes.domainlayer.model.Device;
import bg.tuplovdiv.shazamdes.domainlayer.model.DeviceData;
import bg.tuplovdiv.shazamdes.domainlayer.model.User;
import bg.tuplovdiv.shazamdes.domainlayer.model.UserTO;
import bg.tuplovdiv.shazamdes.domainlayer.model.to.PagedDeviceDataTO;
import bg.tuplovdiv.shazamdes.domainlayer.model.to.PagedDevicesTO;
import bg.tuplovdiv.shazamdes.servicelayer.repository.DeviceDataRepository;
import bg.tuplovdiv.shazamdes.servicelayer.repository.DeviceRepository;
import bg.tuplovdiv.shazamdes.servicelayer.repository.UserRepository;
import bg.tuplovdiv.shazamdes.servicelayer.service.CurrentDeviceDataService;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

/**
 * Created by Nikolay Ivanov on 2018-06-09
 */
public class ClientStepDefinitions extends BaseDefinitions {

    private static final String NEW_USER_PASSWORD = "123456789";

    private static final String DEVICE1_ID = "1234567890";
    private static final String DEVICE2_ID = "1234567891";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private DeviceDataRepository deviceDataRepository;

    @Autowired
    private CurrentDeviceDataService currentDeviceDataService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private List<Device> existingDevices = new ArrayList<>();

    private List<DeviceData> loggedAverageDeviceData = new ArrayList<>();

    private Device expectedDevice;

    private DeviceData expectedCurrentDeviceData;

    @When("^a request for getting the user's profile is sent$")
    public void aRequestForGettingTheUsersProfileIsSent() throws Throwable {
        sendAutheticatedGetRequest(getServerAddressAndPort() + "/client/profile", User.class);
    }

    @And("^the user's profile is returned$")
    public void theUsersProfileIsReturned() throws Throwable {
        User returnedUser = getResponseEntityBodyAsType(User.class);

        assertEquals("User's id is not the same", user.getId(), returnedUser.getId());
        assertEquals("User's email is not the same", user.getEmail(), returnedUser.getEmail());
    }

    @When("^a request for updating the user's password is sent$")
    public void aRequestForUpdatingTheUsersPasswordIsSent() throws Throwable {

        UserTO updatedUser = new UserTO(user.getId(), user.getEmail(), NEW_USER_PASSWORD);

        sendAutheticatedPutRequest(getServerAddressAndPort() + "/client/profile", updatedUser, User.class);
    }

    @And("^the user's password is updated$")
    public void theUsersPasswordIsUpdated() throws Throwable {
        User updatedUser = userRepository.findByEmail(user.getEmail()).orElse(null);
        assertNotNull(updatedUser);

        assertTrue("The new password is not set", passwordEncoder.matches(NEW_USER_PASSWORD, updatedUser.getPassword()));
    }

    @And("^there exist several devices associated to the user$")
    public void thereExistSeveralDevicesAssociatedToTheUser() throws Throwable {
        //Create a device
        Device newDevice = new Device();
        newDevice.setId(DEVICE1_ID);
        newDevice.setName("dev1");
        newDevice.getOwners().add(user);
        newDevice.setCreatedByServer(false);

        newDevice = deviceRepository.save(newDevice);
        assertNotNull(newDevice);

        existingDevices.add(newDevice);

        //Create a second device
        newDevice = new Device();
        newDevice.setId(DEVICE2_ID);
        newDevice.setName("dev2");
        newDevice.getOwners().add(user);
        newDevice.setCreatedByServer(false);

        newDevice = deviceRepository.save(newDevice);
        assertNotNull(newDevice);

        existingDevices.add(newDevice);
    }

    @When("^a request for getting all devices is sent$")
    public void aRequestForGettingAllDevicesIsSent() throws Throwable {
        sendAutheticatedGetRequest(getServerAddressAndPort() + "/client/devices", PagedDevicesTO.class);
    }

    @And("^a list of all devices is returned$")
    public void aListOfAllDevicesIsReturned() throws Throwable {
        PagedDevicesTO returnedDevicePage = getResponseEntityBodyAsType(PagedDevicesTO.class);

        List<Device> returnedDevices = returnedDevicePage.getData();

        assertTrue("The returned devices are not the same as the expected devices",
                returnedDevices.containsAll(existingDevices) && returnedDevices.size() == existingDevices.size());
    }

    @When("^a request for creating a device is sent$")
    public void aRequestForCreatingADeviceIsSent() throws Throwable {
        expectedDevice = new Device();
        expectedDevice.setId(DEVICE1_ID);
        expectedDevice.setName("dev1");

        sendAutheticatedPostRequest(getServerAddressAndPort() + "/client/devices", expectedDevice, Device.class);
    }

    @And("^the new device is returned$")
    public void theNewDeviceIsReturned() throws Throwable {
        theExpectedDeviceIsReturned();
    }

    @And("^the new device is persisted$")
    public void theNewDeviceIsPersisted() throws Throwable {
        theExpectedDeviceIsPersisted();
    }

    @And("^there exists a device associated to the user$")
    public void thereExistsADeviceAssociatedToTheUser() throws Throwable {
        Device newDevice = new Device();
        newDevice.setId(DEVICE1_ID);
        newDevice.setName("dev1");
        newDevice.getOwners().add(user);
        newDevice.setCreatedByServer(false);

        newDevice = deviceRepository.save(newDevice);
        assertNotNull(newDevice);

        expectedDevice = newDevice;
    }

    @When("^a request for getting the device is sent$")
    public void aRequestForGettingTheDeviceIsSent() throws Throwable {
        sendAutheticatedGetRequest(getServerAddressAndPort() + "/client/devices/" + expectedDevice.getId(), Device.class);
    }

    @And("^the expected device is returned$")
    public void theExpectedDeviceIsReturned() throws Throwable {
        Device returnedDevice = getResponseEntityBodyAsType(Device.class);

        assertEquals("Devices do not match", expectedDevice, returnedDevice);
    }

    @When("^a request for updating the device is sent$")
    public void aRequestForUpdatingTheDeviceIsSent() throws Throwable {

        //Update device's name and description
        expectedDevice.setName("device2");
        expectedDevice.setDescription("Description");
        sendAutheticatedPutRequest(getServerAddressAndPort() + "/client/devices/" + expectedDevice.getId(), expectedDevice, Device.class);
    }

    @And("^the updated device is returned$")
    public void theUpdatedDeviceIsReturned() throws Throwable {
        theExpectedDeviceIsReturned();
    }

    @And("^the updated device is persisted$")
    public void theUpdatedDeviceIsPersisted() throws Throwable {
        theExpectedDeviceIsPersisted();
    }

    private void theExpectedDeviceIsPersisted() {
        Device device = deviceRepository.findOne(expectedDevice.getId());
        assertNotNull("Device is not persisted", device);
    }

    @When("^a request for deleting the device is sent$")
    public void aRequestForDeletingTheDeviceIsSent() throws Throwable {
        sendAutheticatedDeleteRequest(getServerAddressAndPort() + "/client/devices/" + expectedDevice.getId());
    }

    @And("^the device is deleted$")
    public void theDeviceIsDeleted() throws Throwable {

    }

    @And("^the user is no more an owner of the device$")
    public void theUserIsNoMoreAnOwnerOfTheDevice() throws Throwable {
        Device device = deviceRepository.findOne(expectedDevice.getId());
        assertNotNull("Device must not be deleted", device);

        assertFalse("User must not be an owner of the device", device.getOwners().contains(user));
    }

    @And("^there is some logged overage data$")
    public void thereIsSomeLoggedOverageData() throws Throwable {
        DeviceData deviceData = new DeviceData(
                UUID.randomUUID().toString(),
                expectedDevice,
                Instant.now(),
                230.1,
                229.1,
                231.1,
                55.5,
                54.5,
                56.5,
                50.0,
                12.6,
                50.1
                );
        deviceData = deviceDataRepository.save(deviceData);

        assertNotNull(deviceData);

        loggedAverageDeviceData.add(deviceData);
    }

    @When("^a request for getting the overage data is sent$")
    public void aRequestForGettingTheOverageDataIsSent() throws Throwable {
        sendAutheticatedGetRequest(getServerAddressAndPort() + "/client/devices/" + expectedDevice.getId() + "/data?fromDate=2018-05-16T05:28:06Z", PagedDeviceDataTO.class);
    }

    @And("^the expected average device data is returned$")
    public void theExpectedAverageDeviceDataIsReturned() throws Throwable {
        PagedDeviceDataTO pagedDeviceDataTO = getResponseEntityBodyAsType(PagedDeviceDataTO.class);

        List<DeviceData> returnedDeviceData = pagedDeviceDataTO.getData();

        assertTrue("The returned device data is not the same as the expected",
                returnedDeviceData.containsAll(loggedAverageDeviceData) && returnedDeviceData.size() == loggedAverageDeviceData.size());
    }

    @And("^there is some logged current data$")
    public void thereIsSomeLoggedCurrentData() throws Throwable {
        expectedCurrentDeviceData = new DeviceData(
                UUID.randomUUID().toString(),
                expectedDevice,
                Instant.now(),
                230.1,
                229.1,
                231.1,
                55.5,
                54.5,
                56.5,
                50.0,
                12.6,
                50.1
        );

        currentDeviceDataService.storeCurrentDataFromDevice(expectedDevice.getId(), expectedCurrentDeviceData);
    }

    @When("^a request for getting the current data is sent$")
    public void aRequestForGettingTheCurrentDataIsSent() throws Throwable {
        sendAutheticatedGetRequest(getServerAddressAndPort() + "/client/devices/" + expectedDevice.getId() + "/data/current", DeviceData.class);
    }

    @And("^the expected current device data is returned$")
    public void theExpectedCurrentDeviceDataIsReturned() throws Throwable {
        DeviceData returnedDeviceData = getResponseEntityBodyAsType(DeviceData.class);

        assertEquals("The returned device data is not the same as expected", expectedCurrentDeviceData, returnedDeviceData);
    }
}
