package bg.tuplovdiv.shazamdes;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by Nikolay Ivanov on 2018-06-09
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        glue = "bg.tuplovdiv.shazamdes.stepdefinition",
        features = "src/test/resources/features",
        format = {"pretty", "html:target/reports/cucumber"},
        strict = true,
        monochrome = true,
        snippets = SnippetType.CAMELCASE
)
public class CucumberIT {
}
