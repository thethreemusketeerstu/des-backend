package bg.tuplovdiv.shazamdes.domainlayer.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Collection;

@Entity
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class User implements UserDetails{

    @Id
    private String id;

    @NotNull
    private String email;

    @JsonIgnore
    @Column(name="password_hash")
    private String passwordHash;

    @Transient
    @JsonProperty(value = "password", access = JsonProperty.Access.WRITE_ONLY)
    private String rawPassword;

    @JsonIgnore
    @Column(name="last_password_reset_date")
    private Instant lastPasswordResetDate;

    @Override
    @Transient
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    @Transient
    @JsonIgnore
    public String getPassword() {
        return this.passwordHash;
    }

    @Override
    @Transient
    @JsonIgnore
    public String getUsername() {
        return this.email;
    }

    @Override
    @Transient
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    @Transient
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    @Transient
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    @Transient
    @JsonIgnore
    public boolean isEnabled() {
        return true;
    }
}
