package bg.tuplovdiv.shazamdes.domainlayer.model.to;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
public class ErrorTO {

    private int statusCode;
    private List<String> errors;

    public ErrorTO(HttpStatus status, List<String> errors) {
        this.statusCode = status.value();
        this.errors = errors;
    }

    public ErrorTO(HttpStatus status, String error){
        this.statusCode = status.value();
        this.errors = new ArrayList<>(1);
        errors.add(error);
    }

}
