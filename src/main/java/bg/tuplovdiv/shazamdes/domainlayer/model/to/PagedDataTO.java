package bg.tuplovdiv.shazamdes.domainlayer.model.to;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

/**
 * Created by Nikolay Ivanov on 2018-05-12
 */
@Getter
@AllArgsConstructor
public class PagedDataTO<T> {

    private List<T> data;

    private PageTO page;
}