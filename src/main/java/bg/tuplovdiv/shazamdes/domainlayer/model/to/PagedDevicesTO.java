package bg.tuplovdiv.shazamdes.domainlayer.model.to;

import bg.tuplovdiv.shazamdes.domainlayer.model.Device;
import lombok.Getter;

import java.util.List;

/**
 * Created by Nikolay Ivanov on 2018-05-12
 */
@Getter
public class PagedDevicesTO extends PagedDataTO<Device> {

    public PagedDevicesTO(List<Device> data, PageTO page) {
        super(data, page);
    }

}