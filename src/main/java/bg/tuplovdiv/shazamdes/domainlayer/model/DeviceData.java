package bg.tuplovdiv.shazamdes.domainlayer.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Entity
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
@AllArgsConstructor
public class DeviceData {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Id
    private String id;

    @ManyToOne
    @JsonIgnore
    private Device device;

    @Column(name="from_date")
    @NotNull
    private Instant fromDate;

    @NotNull
    private Double u1;

    @NotNull
    private Double u2;

    @NotNull
    private Double u3;

    @NotNull
    private Double i1;

    @NotNull
    private Double i2;

    @NotNull
    private Double i3;

    @NotNull
    private Double p;

    @NotNull
    private Double q;

    @NotNull
    private Double f;
}
