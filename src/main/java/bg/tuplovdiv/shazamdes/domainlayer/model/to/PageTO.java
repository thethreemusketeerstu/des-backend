package bg.tuplovdiv.shazamdes.domainlayer.model.to;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by Nikolay Ivanov on 2018-05-12
 */
@Getter
@AllArgsConstructor
public class PageTO {

    private Integer totalItems;

    private Integer totalPages;

    private Integer pageSize;

    private Integer currentPage;

}