package bg.tuplovdiv.shazamdes.domainlayer.model.to;

import bg.tuplovdiv.shazamdes.domainlayer.model.DeviceData;
import lombok.Getter;

import java.util.List;

/**
 * Created by Nikolay Ivanov on 2018-05-12
 */
@Getter
public class PagedDeviceDataTO  extends PagedDataTO<DeviceData> {

    public PagedDeviceDataTO(List<DeviceData> data, PageTO page) {
        super(data, page);
    }

}
