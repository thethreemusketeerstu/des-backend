package bg.tuplovdiv.shazamdes.domainlayer.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@EqualsAndHashCode(exclude = {"owners"})
public class Device {

    @Id
    @NotNull
    @Size(max = 10, min = 10)
    private String id;

    @NotNull
    private String name;

    private String description;

    @JsonIgnore
    @ManyToMany( fetch = FetchType.EAGER)
    @JoinTable(
            name="device_has_owner"
            , joinColumns={
            @JoinColumn(name="device_id")
    }
            , inverseJoinColumns={
            @JoinColumn(name="user_id")
    }
    )
    private List<User> owners = new ArrayList<>();

    @JsonIgnore
    @Column(name="created_by_server")
    private boolean createdByServer;
}
