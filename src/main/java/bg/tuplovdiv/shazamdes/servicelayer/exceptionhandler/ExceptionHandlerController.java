package bg.tuplovdiv.shazamdes.servicelayer.exceptionhandler;

import bg.tuplovdiv.shazamdes.domainlayer.model.to.ErrorTO;
import bg.tuplovdiv.shazamdes.servicelayer.exception.InsufficientPermissionsException;
import bg.tuplovdiv.shazamdes.servicelayer.exception.InvalidEntityException;
import bg.tuplovdiv.shazamdes.servicelayer.exception.ResourceConflictException;
import bg.tuplovdiv.shazamdes.servicelayer.exception.ResourceNotFoundException;
import com.fasterxml.jackson.core.JsonParseException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nikolay Ivanov on 2018-05-05
 */
@RestControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {


    /**
     * Handles exceptions thrown when validation of a field fails
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception, HttpHeaders headers, HttpStatus status, WebRequest request) {

        List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();

        List<String> errors = new ArrayList<>();
        fieldErrors.forEach(fieldError -> errors.add(fieldError.getField() + ": " + fieldError.getDefaultMessage()));

        return createErrorResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY, errors);

    }

    /**
     * Handles exceptions thrown when a resource cannot be found
     */
    @ExceptionHandler(value = {ResourceNotFoundException.class})
    public ResponseEntity<Object> onResourceNotFoundException(Exception exception, WebRequest req) {

        return createErrorResponseEntity(HttpStatus.NOT_FOUND, exception.getMessage());
    }

    /**
     * Handles exceptions thrown when a user does not have permissions to perform a task
     */
    @ExceptionHandler(value = {InsufficientPermissionsException.class})
    public ResponseEntity<Object> onInsufficientPermissionsException(Exception exception, WebRequest req) {

        return createErrorResponseEntity(HttpStatus.FORBIDDEN, exception.getMessage());
    }

    /**
     * Handles exceptions thrown when a resource conflict occurs
     * */
    @ExceptionHandler(value = { ResourceConflictException.class })
    public ResponseEntity<Object> onResourceConflictException(Exception exception, WebRequest req) {

        return createErrorResponseEntity(HttpStatus.CONFLICT, exception.getMessage());
    }

    /**
     * Handles exceptions thrown when a certain manipulation on an object cannot be done,
     * e.g. when trying to update user's email
     */
    @ExceptionHandler(value = {UnsupportedOperationException.class})
    public ResponseEntity<Object> onUnsupportedOperationException(Exception exception, WebRequest req) {

        return createErrorResponseEntity(HttpStatus.CONFLICT, exception.getMessage());
    }

    /**
     * Handles exceptions thrown when a Json object cannot be parsed to a Java object
     */
    @ExceptionHandler(value = {JsonParseException.class})
    public ResponseEntity<Object> onJsonParseException(Exception exception, WebRequest req) {

        return createErrorResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY, "Cannot parse JSON object");
    }

    /**
     * Handles exceptions thrown an entity object contains invalid values for its properties
     * */
    @ExceptionHandler(value = { InvalidEntityException.class, ConstraintViolationException.class})
    public ResponseEntity<Object> onInvalidEntityException (Exception exception, WebRequest req) {

        return createErrorResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY, exception.getMessage());
    }

    /**
     * Handles exceptions not caught by the above methods
     */
    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<Object> onExceptionThrown(Exception exception, WebRequest req) {

         return createErrorResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR, exception.getMessage());
    }


    private ResponseEntity<Object> createErrorResponseEntity(HttpStatus httpStatus, List<String> errors) {

        return new ResponseEntity<>(
                new ErrorTO(httpStatus, errors),
                httpStatus);
    }


    private ResponseEntity<Object> createErrorResponseEntity(HttpStatus httpStatus, String error) {

        return new ResponseEntity<>(
                new ErrorTO(httpStatus, error),
                httpStatus);

    }
}
