package bg.tuplovdiv.shazamdes.servicelayer.exception;

/**
 * Created by Nikolay Ivanov on 2018-05-10
 */
public class ResourceConflictException extends RuntimeException {

    public ResourceConflictException(String message) {
        super(message);
    }
}
