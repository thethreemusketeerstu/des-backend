package bg.tuplovdiv.shazamdes.servicelayer.exception;

/**
 * Created by Nikolay Ivanov on 2018-06-10
 */
public class InsufficientPermissionsException extends RuntimeException {
    public InsufficientPermissionsException(String message) {
        super(message);
    }
}
