package bg.tuplovdiv.shazamdes.servicelayer.exception;

/**
 * Created by Nikolay Ivanov on 2018-05-10
 */
public class InvalidEntityException extends RuntimeException {

    public InvalidEntityException(String message) {
        super(message);
    }
}
