package bg.tuplovdiv.shazamdes.servicelayer.exception;

/**
 * Created by Nikolay Ivanov on 2018-05-05
 */
public class ResourceNotFoundException extends RuntimeException{
    public ResourceNotFoundException(String message) {
        super(message);
    }
}

