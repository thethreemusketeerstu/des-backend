package bg.tuplovdiv.shazamdes.servicelayer.repository;

import bg.tuplovdiv.shazamdes.domainlayer.model.Device;
import bg.tuplovdiv.shazamdes.domainlayer.model.DeviceData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;

/**
 * Created by Nikolay Ivanov on 2018-04-30.
 */

@Repository
public interface DeviceDataRepository extends PagingAndSortingRepository<DeviceData, String> {

    DeviceData findFirstByDeviceIdOrderByFromDateDesc(String deviceId);

    Page<DeviceData> findByDeviceAndFromDateGreaterThanEqual(Device device, Instant fromDate, Pageable pageable);
    Page<DeviceData> findByDeviceAndFromDateBetween(Device device, Instant fromDate, Instant throughDate, Pageable pageable);
}
