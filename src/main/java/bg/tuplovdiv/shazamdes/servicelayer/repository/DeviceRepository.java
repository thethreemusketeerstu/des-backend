package bg.tuplovdiv.shazamdes.servicelayer.repository;

import bg.tuplovdiv.shazamdes.domainlayer.model.Device;
import bg.tuplovdiv.shazamdes.domainlayer.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by Nikolay Ivanov on 2018-05-15.
 */
@Repository
public interface DeviceRepository extends PagingAndSortingRepository<Device, String> {

    Page<Device> findByOwners(User user, Pageable pageable);

    Optional<Device> findById(String deviceId);
}
