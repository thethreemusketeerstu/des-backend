package bg.tuplovdiv.shazamdes.servicelayer.repository;

import bg.tuplovdiv.shazamdes.domainlayer.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by Nikolay Ivanov on 2018-04-30.
 */
@Repository
public interface UserRepository extends CrudRepository<User, String> {

    Optional<User> findByEmail(String email);
}
