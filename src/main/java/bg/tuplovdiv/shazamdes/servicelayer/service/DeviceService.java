package bg.tuplovdiv.shazamdes.servicelayer.service;

import bg.tuplovdiv.shazamdes.domainlayer.model.Device;
import bg.tuplovdiv.shazamdes.domainlayer.model.User;
import bg.tuplovdiv.shazamdes.domainlayer.model.to.PageTO;
import bg.tuplovdiv.shazamdes.domainlayer.model.to.PagedDevicesTO;
import bg.tuplovdiv.shazamdes.servicelayer.exception.InsufficientPermissionsException;
import bg.tuplovdiv.shazamdes.servicelayer.exception.ResourceConflictException;
import bg.tuplovdiv.shazamdes.servicelayer.exception.ResourceNotFoundException;
import bg.tuplovdiv.shazamdes.servicelayer.repository.DeviceRepository;
import bg.tuplovdiv.shazamdes.servicelayer.repository.UserRepository;
import bg.tuplovdiv.shazamdes.servicelayer.security.utils.AuthenticationPrincipal;
import bg.tuplovdiv.shazamdes.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by Nikolay Ivanov on 2018-05-05
 */

@Service
public class DeviceService {

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationPrincipal authenticationPrincipal;

    @Autowired
    private Utils utils;


    public PagedDevicesTO findDevicesForCurrentUser(Pageable pageable) {

        User currentUser = authenticationPrincipal.getCurrentUser();
        Page<Device> devicePage = deviceRepository.findByOwners(currentUser, pageable);

        PageTO pageTO = utils.createPageTOFromPage(devicePage);
        return new PagedDevicesTO(devicePage.getContent(), pageTO);
    }

    public Device getDeviceById(String deviceId) {
        return findByIdOrElseThrowException(deviceId);
    }

    Optional<Device> findById(String deviceId){
        return deviceRepository.findById(deviceId);
    }

    Device findByIdOrElseThrowException(String deviceId) {
        return this.findById(deviceId)
                .orElseThrow(() -> new ResourceNotFoundException("Device with id=" + deviceId + " cannot be found"));
    }

    public Device createDevice(Device newDevice) {
        if(deviceExists(newDevice.getId())){
            Device device = findByIdOrElseThrowException(newDevice.getId());
            if(!device.isCreatedByServer()){
                throw new ResourceConflictException("Device with id = " + newDevice.getId() + " exists");
            }

            return updateServerCreatedDevice(device, newDevice);
        }else {
            return createNewDevice(newDevice);
        }
    }

    private Device createNewDevice(Device newDevice) {
        User currentUser = authenticationPrincipal.getCurrentUser();
        newDevice.getOwners().add(currentUser);
        newDevice.setCreatedByServer(false);
        return deviceRepository.save(newDevice);
    }


    Device createNewServerDevice(String deviceId) {
        Device device = new Device();
        device.setId(deviceId);

        //Set the id as a name
        device.setName(deviceId);

        device.setCreatedByServer(true);
        return deviceRepository.save(device);
    }

    private Device updateServerCreatedDevice(Device oldDevice, Device newDevice) {
        User currentUser = authenticationPrincipal.getCurrentUser();

        oldDevice.setName(newDevice.getName());
        oldDevice.setDescription(newDevice.getDescription());
        oldDevice.setCreatedByServer(false);

        newDevice.getOwners().add(currentUser);
        return deviceRepository.save(newDevice);

    }

    private boolean deviceExists(String deviceId) {
        return findById(deviceId).isPresent();
    }

    public Device updateDevice(Device updatedDevice) {
        Device device = findByIdOrElseThrowException(updatedDevice.getId());

        //If the user is not an owner of the device throw exception
        if(!isCurrentUserOwnerOfDevice(device)){
            throw new InsufficientPermissionsException("User is not an owner of the device");
        }
        device.setName(updatedDevice.getName());
        device.setDescription(updatedDevice.getDescription());

        return deviceRepository.save(device);
    }

    public void deleteDevice(String deviceId) {
        Device device = findByIdOrElseThrowException(deviceId);
        User currentUser = authenticationPrincipal.getCurrentUser();

        // If the user is not an owner of the device throw exception
        if(!isCurrentUserOwnerOfDevice(device)){
            throw new InsufficientPermissionsException("User is not an owner of the device");
        }

        device.getOwners().remove(currentUser);

        // If the device has no more owners mark it as created by the server
        // so that a user can create it again
        if(device.getOwners().isEmpty()){
            device.setCreatedByServer(true);
        }
        deviceRepository.save(device);
    }

    boolean isCurrentUserOwnerOfDevice(Device device){
        User currentUser = authenticationPrincipal.getCurrentUser();
        return device.getOwners().contains(currentUser);
    }
}
