package bg.tuplovdiv.shazamdes.servicelayer.service;

import bg.tuplovdiv.shazamdes.domainlayer.model.User;
import bg.tuplovdiv.shazamdes.servicelayer.repository.UserRepository;
import bg.tuplovdiv.shazamdes.servicelayer.security.utils.AuthenticationPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationPrincipal authenticationPrincipal;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }


    public User getCurrentUser() {
        return authenticationPrincipal.getCurrentUser();
    }

    public User updateCurrentUser(User user) {
        User currentUser = authenticationPrincipal.getCurrentUser();

        if(!user.getEmail().equals(currentUser.getEmail())){
            throw new UnsupportedOperationException("Cannot update user email");
        }

        if(user.getRawPassword() != null){
            currentUser.setPasswordHash(passwordEncoder.encode(user.getRawPassword()));
            currentUser.setLastPasswordResetDate(Instant.now());
        }

        currentUser = userRepository.save(currentUser);
        return currentUser;
    }
}
