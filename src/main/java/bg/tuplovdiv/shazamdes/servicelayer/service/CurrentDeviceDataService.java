package bg.tuplovdiv.shazamdes.servicelayer.service;

import bg.tuplovdiv.shazamdes.domainlayer.model.Device;
import bg.tuplovdiv.shazamdes.domainlayer.model.DeviceData;
import bg.tuplovdiv.shazamdes.servicelayer.exception.InsufficientPermissionsException;
import bg.tuplovdiv.shazamdes.servicelayer.exception.ResourceNotFoundException;
import bg.tuplovdiv.shazamdes.servicelayer.security.utils.AuthenticationPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * <p>A service class that provides methods for storing and retrieving real-time data provided by devices.
 * The implementation uses an in-memory cache, since only the average data needs to be persisted in a database.</p>
 * <p>Created by Nikolay Ivanov on 2018-05-12</p>
 */

@Service
public class CurrentDeviceDataService {

    private Map<String, DeviceData> currentDataMap = new HashMap<>();

    @Autowired
    private DeviceDataService deviceDataService;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private AuthenticationPrincipal authenticationPrincipal;

    /**
     * Returns the latest data sent by the device with id = deviceId.
     * The method first checks if there is any data in the private cache
     * and then queries the database for the latest average data.
     * Finally, if there is no data for this device returns null.
     * @throws ResourceNotFoundException if a device with the provided id does not exist
     */
    public DeviceData getLatestDataForDevice(String deviceId) {
        Device device = deviceService.findByIdOrElseThrowException(deviceId);

        //If the user is not an owner of the device throw exception
        if(!deviceService.isCurrentUserOwnerOfDevice(device)){
            throw new InsufficientPermissionsException("User is not an owner of the device");
        }
        DeviceData data = currentDataMap.get(deviceId);
        if (data == null) {
            data = deviceDataService.getLatestAverageDataForDevice(device);
        }
        return data;
    }


    public void storeCurrentDataFromDevice(String deviceId, DeviceData deviceData) {
        Device device = deviceService.findById(deviceId).orElseGet(() -> deviceService.createNewServerDevice(deviceId));

        deviceData.setDevice(device);
        //Set a random id
        deviceData.setId(UUID.randomUUID().toString());
        this.currentDataMap.put(deviceId, deviceData);
    }
}
