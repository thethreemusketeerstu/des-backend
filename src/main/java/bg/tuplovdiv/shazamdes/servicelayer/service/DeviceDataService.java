package bg.tuplovdiv.shazamdes.servicelayer.service;

import bg.tuplovdiv.shazamdes.domainlayer.model.Device;
import bg.tuplovdiv.shazamdes.domainlayer.model.DeviceData;
import bg.tuplovdiv.shazamdes.domainlayer.model.to.PageTO;
import bg.tuplovdiv.shazamdes.domainlayer.model.to.PagedDeviceDataTO;
import bg.tuplovdiv.shazamdes.servicelayer.exception.InsufficientPermissionsException;
import bg.tuplovdiv.shazamdes.servicelayer.repository.DeviceDataRepository;
import bg.tuplovdiv.shazamdes.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

/**
 * Created by Nikolay Ivanov on 2018-05-10
 */

@Service
public class DeviceDataService {

    @Autowired
    private DeviceDataRepository deviceDataRepository;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private Utils utils;

    DeviceData getLatestAverageDataForDevice(Device device) {
        return deviceDataRepository.findFirstByDeviceIdOrderByFromDateDesc(device.getId());
    }

    public void storeDataFromDevice(String deviceId, DeviceData deviceData) {
        Device device = deviceService.findById(deviceId).orElseGet(() -> deviceService.createNewServerDevice(deviceId));
        this.storeDeviceData(device, deviceData);
    }


    public void storeBulkDataFromDevice(String deviceId, List<DeviceData> deviceData) {
        Device device = deviceService.findById(deviceId).orElseGet(() -> deviceService.createNewServerDevice(deviceId));
        deviceData.forEach(data -> this.storeDeviceData(device, data));
    }

    private void storeDeviceData(Device device, DeviceData deviceData){
        deviceData.setDevice(device);
        //Set a random id
        deviceData.setId(UUID.randomUUID().toString());
        deviceDataRepository.save(deviceData);
    }

    public PagedDeviceDataTO getDeviceData(String deviceId, Instant fromDate, Instant throughDate, Pageable pageable) {

        Device device = deviceService.findByIdOrElseThrowException(deviceId);

        //If the user is not an owner of the device throw exception
        if(!deviceService.isCurrentUserOwnerOfDevice(device)){
            throw new InsufficientPermissionsException("User is not an owner of the device");
        }

        Page<DeviceData> deviceDataPage;
        if(throughDate == null){
            deviceDataPage = deviceDataRepository.findByDeviceAndFromDateGreaterThanEqual(device, fromDate, pageable);
        } else {
            deviceDataPage = deviceDataRepository.findByDeviceAndFromDateBetween(device, fromDate, throughDate, pageable);
        }

        PageTO pageTO = utils.createPageTOFromPage(deviceDataPage);
        return new PagedDeviceDataTO(deviceDataPage.getContent(), pageTO);
    }
}
