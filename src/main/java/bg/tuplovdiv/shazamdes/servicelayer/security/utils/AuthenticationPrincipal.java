package bg.tuplovdiv.shazamdes.servicelayer.security.utils;

import bg.tuplovdiv.shazamdes.domainlayer.model.User;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * Created by Nikolay Ivanov on 2017-12-27.
 */
@Component
public class AuthenticationPrincipal {

    public User getCurrentUser(){
        try {
            return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        }catch (Exception e){
            return null;
        }
    }

    public String getCurrentDeviceId(){
        try {
            return (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        }catch (Exception e){
            return null;
        }
    }

}
