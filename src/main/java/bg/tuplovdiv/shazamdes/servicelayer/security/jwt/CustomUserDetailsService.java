package bg.tuplovdiv.shazamdes.servicelayer.security.jwt;

import bg.tuplovdiv.shazamdes.domainlayer.model.User;
import bg.tuplovdiv.shazamdes.servicelayer.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;


    @Override
    public User loadUserByUsername(String email) throws UsernameNotFoundException {
        return userService.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("User not found!"));
    }

}