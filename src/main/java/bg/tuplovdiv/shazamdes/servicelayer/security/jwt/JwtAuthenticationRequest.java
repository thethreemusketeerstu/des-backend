package bg.tuplovdiv.shazamdes.servicelayer.security.jwt;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class  JwtAuthenticationRequest implements Serializable {

    private static final long serialVersionUID = 3256198213464632L;

    private String email;
    private String password;

}