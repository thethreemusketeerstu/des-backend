package bg.tuplovdiv.shazamdes.servicelayer.security.jwt;


import bg.tuplovdiv.shazamdes.domainlayer.model.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;


@Getter
@Setter
@AllArgsConstructor
public class JwtAuthenticationResponse implements Serializable {

    private static final long serialVersionUID = 1250166508152483573L;

    private String token;
    private User user;

}