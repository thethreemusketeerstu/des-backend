package bg.tuplovdiv.shazamdes.servicelayer.security.jwt;

import bg.tuplovdiv.shazamdes.domainlayer.model.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mobile.device.Device;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class JwtTokenUtil implements Serializable {

    private static final long serialVersionUID = -3301605591108950415L;

    static final String CLAIM_KEY_USERNAME = "sub";
    static final String CLAIM_KEY_DEVICE_ID = "deviceId";
    static final String CLAIM_KEY_AUDIENCE = "audience";
    static final String CLAIM_KEY_CREATED = "created";

    private static final String AUDIENCE_UNKNOWN = "unknown";
    private static final String AUDIENCE_WEB = "web";
    private static final String AUDIENCE_MOBILE = "mobile";
    private static final String AUDIENCE_TABLET = "tablet";
    private static final String AUDIENCE_EMBEDDED = "embedded";

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expiration}")
    private Long expiration;

    public String getUsernameFromToken(String token) {
        String username;
        try {
            final Claims claims = getClaimsFromToken(token);
            username = claims.getSubject();
        } catch (Exception e) {
            username = null;
        }
        return username;
    }

    public String getDeviceIdFromToken(String token) {
        String deviceId;
        try {
            final Claims claims = getClaimsFromToken(token);
            deviceId = (String) claims.get(CLAIM_KEY_DEVICE_ID);
        } catch (Exception e) {
            deviceId = null;
        }
        return deviceId;
    }

    public Instant getCreatedDateFromToken(String token) {
        final Claims claims = getClaimsFromToken(token);
        return Instant.parse((String)claims.get(CLAIM_KEY_CREATED));
    }

    public Instant getExpirationDateFromToken(String token) {
        final Claims claims = getClaimsFromToken(token);
        return claims.getExpiration().toInstant();
    }

    public String getAudienceFromToken(String token) {
        String audience;
        try {
            final Claims claims = getClaimsFromToken(token);
            audience = (String) claims.get(CLAIM_KEY_AUDIENCE);
        } catch (Exception e) {
            audience = null;
        }
        return audience;
    }

    private Claims getClaimsFromToken(String token) {
        Claims claims;
        try {
            claims = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e) {
            claims = null;
        }
        return claims;
    }

    private Instant generateExpirationDate() {
        return Instant.ofEpochMilli(System.currentTimeMillis() + expiration * 1000);
    }

    private Boolean isTokenExpired(String token) {
        final Instant expiration = getExpirationDateFromToken(token);
        return expiration.isBefore(Instant.now());
    }

    private Boolean isCreatedBeforeLastPasswordReset(Instant created, Instant lastPasswordReset) {
        return (lastPasswordReset != null && created.isBefore(lastPasswordReset));
    }

    private String generateAudience(Device device) {
        String audience = AUDIENCE_UNKNOWN;
        if(device == null){
            return audience;
        }
        if (device.isNormal()) {
            audience = AUDIENCE_WEB;
        } else if (device.isTablet()) {
            audience = AUDIENCE_TABLET;
        } else if (device.isMobile()) {
            audience = AUDIENCE_MOBILE;
        }
        return audience;
    }

    private Boolean ignoreTokenExpiration(String token) {
        String audience = getAudienceFromToken(token);
        return (AUDIENCE_TABLET.equals(audience) || AUDIENCE_MOBILE.equals(audience));
    }

    public String generateToken(UserDetails userDetails, Device device) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(CLAIM_KEY_USERNAME, userDetails.getUsername());
        claims.put(CLAIM_KEY_AUDIENCE, generateAudience(device));
        claims.put(CLAIM_KEY_CREATED,  Instant.now().toString());
        return generateToken(claims);
    }

    private String generateToken(Map<String, Object> claims) {
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(Date.from(generateExpirationDate()))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    public String generateTokenForEmbeddedDevice(String deviceId) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(CLAIM_KEY_DEVICE_ID, deviceId);
        claims.put(CLAIM_KEY_AUDIENCE, AUDIENCE_EMBEDDED);
        claims.put(CLAIM_KEY_CREATED, Instant.now());
        return generateTokenForEmbeddedDevice(claims);
    }

    private String generateTokenForEmbeddedDevice(Map<String, Object> claims) {
        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    public Boolean canTokenBeRefreshed(String token, UserDetails userDetails) {

        User user = (User) userDetails;
        final Instant created = getCreatedDateFromToken(token);

        return (!isTokenExpired(token) || ignoreTokenExpiration(token))
                && !isCreatedBeforeLastPasswordReset(created, user.getLastPasswordResetDate());
    }

    public String refreshToken(String token) {
        String refreshedToken;
        try {
            final Claims claims = getClaimsFromToken(token);
            claims.put(CLAIM_KEY_CREATED, Instant.now());
            refreshedToken = generateToken(claims);
        } catch (Exception e) {
            refreshedToken = null;
        }
        return refreshedToken;
    }

    public Boolean validateTokenFromClient(String token, UserDetails userDetails) {
        User user = (User) userDetails;
        final String username = getUsernameFromToken(token);
        final Instant created = getCreatedDateFromToken(token);
        return (
                username.equals(user.getUsername())
                        && !isTokenExpired(token)
                        && !isCreatedBeforeLastPasswordReset(created, user.getLastPasswordResetDate()));
    }


    public Boolean validateTokenFromDevice(String token, String deviceId) {
        final String extractedDeviceId = getDeviceIdFromToken(token);
        return extractedDeviceId != null && extractedDeviceId.equalsIgnoreCase(deviceId);
    }

}