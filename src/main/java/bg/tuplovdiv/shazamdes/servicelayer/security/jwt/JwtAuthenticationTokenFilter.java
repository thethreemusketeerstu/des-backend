package bg.tuplovdiv.shazamdes.servicelayer.security.jwt;

import bg.tuplovdiv.shazamdes.domainlayer.model.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private CustomUserDetailsService userDetailsService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Value("${jwt.header}")
    private String tokenHeader;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        String authToken = request.getHeader(this.tokenHeader);
        String username = null;
        String deviceId = null;

        String path = request.getServletPath();
        boolean isDevice = path.startsWith("/device");
        if(isDevice){
            String[] subpaths = path.split("/");

            //Assert there is a device id
            //the array must have at least 3 elements - "/", "device", "{deviceId}", ...
            if(subpaths.length > 2){
                deviceId = subpaths[2];
            }

        }else {
            username = jwtTokenUtil.getUsernameFromToken(authToken);
        }

        if (!isDevice && username != null && SecurityContextHolder.getContext().getAuthentication() == null) {

            User userDetails = this.userDetailsService.loadUserByUsername(username);

            // For simple validation it is completely sufficient to just check the token integrity.
            if (jwtTokenUtil.validateTokenFromClient(authToken, userDetails)) {
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }else if (isDevice && deviceId != null && SecurityContextHolder.getContext().getAuthentication() == null ){

            // For simple validation it is completely sufficient to just check the token integrity.
            if (jwtTokenUtil.validateTokenFromDevice(authToken, deviceId)) {
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(deviceId, null, null);
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }

        chain.doFilter(request, response);
    }
}