package bg.tuplovdiv.shazamdes.utils;

import bg.tuplovdiv.shazamdes.domainlayer.model.to.PageTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

/**
 * Created by Nikolay Ivanov on 2018-05-12
 */
@Component
public class Utils {

    @Value("${pagination.defaultPageSize}")
    private Integer defaultPageSize;

    @Value("${pagination.defaultPageNumber}")
    private Integer defaultPageNumber;

    private Utils() {
    }

    public Pageable createPageable(Integer pageNumber, Integer pageSize){

        if(pageNumber == null){
            pageNumber = defaultPageNumber;
        }
        if(pageSize == null){
            pageSize = defaultPageSize;
        }

        return new PageRequest(pageNumber - 1, pageSize);

    }

    public PageTO createPageTOFromPage(Page<?> page){
        return new PageTO(
                Math.toIntExact(page.getTotalElements()),
                page.getTotalPages(),
                page.getSize(),
                page.getNumber() + 1
        );
    }
}
