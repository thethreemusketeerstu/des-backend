package bg.tuplovdiv.shazamdes.restlayer.controller.client;

import bg.tuplovdiv.shazamdes.domainlayer.model.User;
import bg.tuplovdiv.shazamdes.domainlayer.model.to.ErrorTO;
import bg.tuplovdiv.shazamdes.servicelayer.security.jwt.CustomUserDetailsService;
import bg.tuplovdiv.shazamdes.servicelayer.security.jwt.JwtAuthenticationRequest;
import bg.tuplovdiv.shazamdes.servicelayer.security.jwt.JwtAuthenticationResponse;
import bg.tuplovdiv.shazamdes.servicelayer.security.jwt.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Created by Nikolay Ivanov on 2018-04-27
 */

@RestController
@RequestMapping(produces = {"application/json"}, consumes = {"application/json"})
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Value("${jwt.header}")
    private String authenticationHeader;

    @Autowired
    private CustomUserDetailsService userDetailsService;


    @RequestMapping(value = "/client/login", method = RequestMethod.POST)
    public ResponseEntity<?> login(@RequestBody JwtAuthenticationRequest authenticationRequest, Device device){

        try{
            // Perform the security
            final Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            authenticationRequest.getEmail(),
                            authenticationRequest.getPassword()
                    )
            );

            SecurityContextHolder.getContext().setAuthentication(authentication);

            // Reload password post-security so we can generate token
            final User userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getEmail());
            final String token = jwtTokenUtil.generateToken(userDetails, device);

            // Return the token
            return ResponseEntity.ok(new JwtAuthenticationResponse(token, userDetails));
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(
                    new ErrorTO(HttpStatus.UNAUTHORIZED, "Invalid username or password"),
                    HttpStatus.UNAUTHORIZED
            );
        }
    }

    @RequestMapping(value = "/client/tokenRefresh", method = RequestMethod.POST)
    public ResponseEntity<?> refreshAndGetAuthenticationToken(@RequestBody Map<String, String> jsonBody) {

        String token = jsonBody.get("token");

        if (token != null) {

            String email = jwtTokenUtil.getUsernameFromToken(token);
            User userDetails = userDetailsService.loadUserByUsername(email);
            if(jwtTokenUtil.canTokenBeRefreshed(token, userDetails)){
                String refreshedToken = jwtTokenUtil.refreshToken(token);
                return ResponseEntity.ok(new JwtAuthenticationResponse(refreshedToken, userDetails));
            }
        }
        ErrorTO errorTO = new ErrorTO(HttpStatus.BAD_REQUEST, "Token is invalid or cannot be refreshed");
        return ResponseEntity.badRequest().body(errorTO);
    }

}
