package bg.tuplovdiv.shazamdes.restlayer.controller.client;

import bg.tuplovdiv.shazamdes.domainlayer.model.User;
import bg.tuplovdiv.shazamdes.servicelayer.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Created by Nikolay Ivanov on 2018-04-26
 */

@RestController
@Validated
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/client/profile", method = RequestMethod.GET)
    public ResponseEntity<?> getProfile(){

        return ResponseEntity.ok(userService.getCurrentUser());
    }
    @RequestMapping(value = "/client/profile", method = RequestMethod.PUT)
    public ResponseEntity<?> putProfile(@Valid @RequestBody User user){

        return ResponseEntity.ok(userService.updateCurrentUser(user));
    }
}
