package bg.tuplovdiv.shazamdes.restlayer.controller.device;

import bg.tuplovdiv.shazamdes.domainlayer.model.DeviceData;
import bg.tuplovdiv.shazamdes.servicelayer.service.CurrentDeviceDataService;
import bg.tuplovdiv.shazamdes.servicelayer.service.DeviceDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created by Nikolay Ivanov on 2018-04-26
 */

@RestController
@RequestMapping(produces = {"application/json"}, consumes = {"application/json"})
@Validated
public class    DeviceDataController {

    @Autowired
    private DeviceDataService deviceDataService;

    @Autowired
    private CurrentDeviceDataService currentDeviceDataService;

    @RequestMapping(value = "/device/{deviceId}/data/current", method = RequestMethod.POST)
    public ResponseEntity<?> storeCurrentData(@PathVariable("deviceId") @NotNull String deviceId,
                                       @Valid @RequestBody DeviceData deviceData){

        currentDeviceDataService.storeCurrentDataFromDevice(deviceId, deviceData);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/device/{deviceId}/data/average", method = RequestMethod.POST)
    public ResponseEntity<?> storeAverageData(@PathVariable("deviceId") @NotNull String deviceId,
                                       @Valid @RequestBody DeviceData deviceData){

        deviceDataService.storeDataFromDevice(deviceId, deviceData);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/device/{deviceId}/data/average/bulk", method = RequestMethod.POST)
    public ResponseEntity<?> storeAverageDataInBulk(@PathVariable("deviceId") @NotNull String deviceId,
                                                    @Valid @RequestBody @Size(min = 1) List<DeviceData> deviceData){

        deviceDataService.storeBulkDataFromDevice(deviceId, deviceData);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
