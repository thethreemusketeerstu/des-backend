package bg.tuplovdiv.shazamdes.restlayer.controller.client;

import bg.tuplovdiv.shazamdes.domainlayer.model.Device;
import bg.tuplovdiv.shazamdes.domainlayer.model.to.PagedDeviceDataTO;
import bg.tuplovdiv.shazamdes.servicelayer.exception.InvalidEntityException;
import bg.tuplovdiv.shazamdes.servicelayer.service.CurrentDeviceDataService;
import bg.tuplovdiv.shazamdes.servicelayer.service.DeviceDataService;
import bg.tuplovdiv.shazamdes.servicelayer.service.DeviceService;
import bg.tuplovdiv.shazamdes.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.Instant;

/**
 * Created by Nikolay Ivanov on 2018-04-26
 */

@RestController
@Validated
public class DeviceController {

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private DeviceDataService deviceDataService;

    @Autowired
    private CurrentDeviceDataService currentDeviceDataService;

    @Autowired
    private Utils utils;

    @RequestMapping(value = "/client/devices", method = RequestMethod.GET)
    public ResponseEntity<?> getDevices(@RequestParam(value = "pageSize", required = false) @Min(value = 1, message = "Page size must be greater than 0") Integer pageSize,
                                        @RequestParam(value = "pageNumber", required = false) @Min(value = 1, message = "Page number must be greater than 0") Integer pageNumber) {

        Pageable pageable = utils.createPageable(pageNumber, pageSize);

        return ResponseEntity.ok(deviceService.findDevicesForCurrentUser(pageable));

    }
    @RequestMapping(value = "/client/devices/{deviceId}", method = RequestMethod.GET)
    public ResponseEntity<?> getDeviceById(@PathVariable("deviceId") @NotNull String deviceId){

        return ResponseEntity.ok(deviceService.getDeviceById(deviceId));
    }
    @RequestMapping(value = "/client/devices", method = RequestMethod.POST)
    public ResponseEntity<?> postDevice(@RequestBody @Valid Device device){

        return ResponseEntity.ok(deviceService.createDevice(device));
    }
    @RequestMapping(value = "/client/devices/{deviceId}", method = RequestMethod.PUT)
    public ResponseEntity<?> putDeviceById(@PathVariable("deviceId") @NotNull String deviceId,
                                           @RequestBody @Valid Device device){

        //if the deviceId param is not the same as the id of the device
        if(!deviceId.equals(device.getId())){
            throw new InvalidEntityException("Invalid id for device");
        }

        return ResponseEntity.ok(deviceService.updateDevice(device));
    }
    @RequestMapping(value = "/client/devices/{deviceId}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteDeviceById(@PathVariable("deviceId") @NotNull String deviceId){

        deviceService.deleteDevice(deviceId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/client/devices/{deviceId}/data", method = RequestMethod.GET)
    public ResponseEntity<?> getDeviceData(@PathVariable("deviceId") @NotNull String deviceId,
                                           @RequestParam(value = "pageSize", required = false) @Min(value = 1, message = "Page size must be greater than 0") Integer pageSize,
                                           @RequestParam(value = "pageNumber", required = false) @Min(value = 1, message = "Page number must be greater than 0") Integer pageNumber,
                                           @RequestParam(value = "fromDate", required = false) @NotNull Instant fromDate,
                                           @RequestParam(value = "throughDate", required = false) Instant throughDate){

        Pageable pageable = utils.createPageable(pageNumber, pageSize);

        PagedDeviceDataTO pagedDeviceDataTO = deviceDataService.getDeviceData(deviceId, fromDate, throughDate, pageable);

        return ResponseEntity.ok(pagedDeviceDataTO);
    }

    @RequestMapping(value = "/client/devices/{deviceId}/data/current", method = RequestMethod.GET)
    public ResponseEntity<?> getDeviceDataCurrent(@PathVariable("deviceId") @NotNull String deviceId){

        return ResponseEntity.ok(currentDeviceDataService.getLatestDataForDevice(deviceId));
    }
}
