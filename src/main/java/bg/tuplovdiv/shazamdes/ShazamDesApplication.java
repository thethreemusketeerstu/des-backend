package bg.tuplovdiv.shazamdes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShazamDesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShazamDesApplication.class, args);
	}
}
