-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema des-backend-db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema des-backend-db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `des-backend-db` DEFAULT CHARACTER SET utf8 ;
USE `des-backend-db` ;

-- -----------------------------------------------------
-- Table `des-backend-db`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `des-backend-db`.`user` (
  `id` CHAR(36) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `password_hash` CHAR(60) NOT NULL,
  `last_password_reset_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `des-backend-db`.`device`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `des-backend-db`.`device` (
  `id` CHAR(10) NOT NULL,
  `name` VARCHAR(90) NOT NULL,
  `description` VARCHAR(255) NULL,
  `created_by_server` TINYINT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `des-backend-db`.`device_data`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `des-backend-db`.`device_data` (
  `id` CHAR(36) NOT NULL,
  `from_date` DATETIME NOT NULL,
  `u1` DOUBLE NOT NULL,
  `u2` DOUBLE NOT NULL,
  `u3` DOUBLE NOT NULL,
  `i1` DOUBLE NOT NULL,
  `i2` DOUBLE NOT NULL,
  `i3` DOUBLE NOT NULL,
  `p` DOUBLE NOT NULL,
  `q` DOUBLE NOT NULL,
  `f` DOUBLE NOT NULL,
  `device_id` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_device_data_device1_idx` (`device_id` ASC),
  CONSTRAINT `fk_device_data_device1`
    FOREIGN KEY (`device_id`)
    REFERENCES `des-backend-db`.`device` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `des-backend-db`.`device_has_owner`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `des-backend-db`.`device_has_owner` (
  `user_id` CHAR(36) NOT NULL,
  `device_id` CHAR(36) NOT NULL,
  PRIMARY KEY (`user_id`, `device_id`),
  INDEX `fk_user_has_device_device1_idx` (`device_id` ASC),
  INDEX `fk_user_has_device_user_idx` (`user_id` ASC),
  CONSTRAINT `fk_user_has_device_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `des-backend-db`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_device_device1`
    FOREIGN KEY (`device_id`)
    REFERENCES `des-backend-db`.`device` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `des-backend-db`.`user`
-- -----------------------------------------------------
START TRANSACTION;
USE `des-backend-db`;
INSERT INTO `des-backend-db`.`user` (`id`, `email`, `password_hash`, `last_password_reset_date`) VALUES ('0123456789abcdef0123456789abcdef0123', 'thethreemusketeers@des.net', '$2a$10$C6j7GR1.BaUeR70xgTF2HeK1P8KSKceAWzsP0enOFJ/Y.TI0f7BHK', '2018-05-15 00:00:00');

COMMIT;

