    echo "Update package lists..."
    sudo apt-get update

    echo "Install Java 8"
    sudo apt-get install -y software-properties-common python-software-properties
    echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections
    sudo add-apt-repository ppa:webupd8team/java -y
    sudo apt-get update
    sudo apt-get install oracle-java8-installer

    echo "Set environment variables for Java 8"
    sudo apt-get install -y oracle-java8-set-default

    echo "Install Maven"
    sudo apt-get install -y maven

    echo "mySql default setup"
    export LC_ALL="en_US.UTF-8"
    sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
    sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
    echo "mySql install"
    sudo apt-get -y install mysql-server
    sed -i "s/^bind-address/#bind-address/" /etc/mysql/my.cnf
    mysql -u root -proot -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root' WITH GRANT OPTION; FLUSH PRIVILEGES; SET GLOBAL max_connect_errors=10000;"
    sudo /etc/init.d/mysql restart
