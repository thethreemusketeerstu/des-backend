# DES-BACKEND
This repository contains the backend part of the Shazam DES project. The backend server is a Spring Boot project. It accepts requests from des-embedded and des-frontend and manages data persistence.

## Running locally

### Prerequisites
In order to run the application you need to have [jdk 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) or later version and [Apache Maven](https://maven.apache.org/install.html) installed on your machine.

A running database server is also required. The database connection can be configured in the `application.properties` files located under {projectBaseDirectory}\src\main\resources.
A database initialization script for MySql DBMS can be found here: {projectBaseDirectory}\src\main\resources\scripts\db-generation-script.sql

### Running
You can start the server with:

`mvn spring-boot:run`

If not configured otherwise the server will listen on `localhost:8080`

### Accessing the server from embedded devices
In order for the devices to access the server they must have a preshared authorization token which is dependant on the device id.
For test purposes here are several authorization tokens and the ids they relate to:

| DEVICE ID  | AUTHORIZATION CODE
|------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1234567890 | eyJhbGciOiJIUzUxMiJ9.eyJhdWRpZW5jZSI6ImVtYmVkZGVkIiwiY3JlYXRlZCI6eyJlcG9jaFNlY29uZCI6MTUyNzMzODIxMywibmFubyI6MzYwMDAwMDB9LCJkZXZpY2VJZCI6IjEyMzQ1Njc4OTAifQ.kWAvDtZfd9bnTXfZMC2oFdeN6eio037R-xq5k76J-njMciB5pX8EpVH60gAETOtMCFgoUPLSGBQkJCgvLbigHg  |
| ABCDEF1234 | eyJhbGciOiJIUzUxMiJ9.eyJhdWRpZW5jZSI6ImVtYmVkZGVkIiwiY3JlYXRlZCI6eyJlcG9jaFNlY29uZCI6MTUyNzQwMDE2MCwibmFubyI6NzM1MDAwMDAwfSwiZGV2aWNlSWQiOiJBQkNERUYxMjM0In0.Fn8vV4RhFOOpIrRuUKkdMo-Rut4MxWn7pMzr7XL7vaZ3DdmaVKGXdbszK3-4sBj8A7NWu9l4LBQhfFWqOobNjw |
| ABCDEF1235 | eyJhbGciOiJIUzUxMiJ9.eyJhdWRpZW5jZSI6ImVtYmVkZGVkIiwiY3JlYXRlZCI6eyJlcG9jaFNlY29uZCI6MTUyNzQwMDM1MSwibmFubyI6NDI2MDAwMDAwfSwiZGV2aWNlSWQiOiJBQkNERUYxMjM1In0.AN3E9BBwoTszsZYqUdmCevbFgZkPs_Vpz5rxx7Ngml_KSZz7oQeXs5SB6MtT5mJPlSZkyW060vEch2PjhUdq5w |
| ABCDEF1236 | eyJhbGciOiJIUzUxMiJ9.eyJhdWRpZW5jZSI6ImVtYmVkZGVkIiwiY3JlYXRlZCI6eyJlcG9jaFNlY29uZCI6MTUyNzQwMDM2OSwibmFubyI6NzAzMDAwMDAwfSwiZGV2aWNlSWQiOiJBQkNERUYxMjM2In0.0JvCGs_hrHuKVLFU1Yks63rDKLJbZQVG2IKw7HBm2QTOYTsgivYTkVuwPmV3IBVgqXNTF6i5UvYGQjr2MisARw |
| ABCDEF1237 | eyJhbGciOiJIUzUxMiJ9.eyJhdWRpZW5jZSI6ImVtYmVkZGVkIiwiY3JlYXRlZCI6eyJlcG9jaFNlY29uZCI6MTUyNzQwMDM5MiwibmFubyI6NjgxMDAwMDAwfSwiZGV2aWNlSWQiOiJBQkNERUYxMjM3In0.RcQeFWfj9N4Hj3O7dpVFYWOi8CzwjPqFiu8aYtXISsYPPdk1TPcdaF9GO-ubVhfcWIqAhs9_1deli2sNdMkx5A |
| ABCDEF1238 | eyJhbGciOiJIUzUxMiJ9.eyJhdWRpZW5jZSI6ImVtYmVkZGVkIiwiY3JlYXRlZCI6eyJlcG9jaFNlY29uZCI6MTUyNzQwMDQzNywibmFubyI6Mjk4MDAwMDAwfSwiZGV2aWNlSWQiOiJBQkNERUYxMjM4In0.7kgcBpJEdUHylXMlsWFdfOPHzHDkbT_w7LylyWjm3-4cBOxq7Hdwv2NpPaYVVtcPSSBETWhEtdFQ_2uo8sp18w |
| ABCDEF1239 | eyJhbGciOiJIUzUxMiJ9.eyJhdWRpZW5jZSI6ImVtYmVkZGVkIiwiY3JlYXRlZCI6eyJlcG9jaFNlY29uZCI6MTUyNzQwMDQ1NywibmFubyI6NTQwMDAwMDB9LCJkZXZpY2VJZCI6IkFCQ0RFRjEyMzkifQ.prtN-akrIvHAiVFI8zsm7FM3mhRUeNQAELLNLm4XmGHw0_mE_-Jv40_I4zWl2XEr9pn0l5suIGMOhxAz6gMkkw  |
| ABCDEF1230 | eyJhbGciOiJIUzUxMiJ9.eyJhdWRpZW5jZSI6ImVtYmVkZGVkIiwiY3JlYXRlZCI6eyJlcG9jaFNlY29uZCI6MTUyNzQwMDQ3MiwibmFubyI6NDM2MDAwMDAwfSwiZGV2aWNlSWQiOiJBQkNERUYxMjMwIn0.d5py9k_kfkGc5-md6_5YgFhTejoRbto4wsUX-yxem9ZcM2JeEy3Ziby13WkoGNVqkSSv3h9aZ3o1DVH_3ayD_g |
| ABCDEF123A | eyJhbGciOiJIUzUxMiJ9.eyJhdWRpZW5jZSI6ImVtYmVkZGVkIiwiY3JlYXRlZCI6eyJlcG9jaFNlY29uZCI6MTUyNzQwMDUwNCwibmFubyI6NjIzMDAwMDAwfSwiZGV2aWNlSWQiOiJBQkNERUYxMjNBIn0.Mddn2Ai1mbCZEuJzXJ652aDpBJrId5W73IzHiPgAoLnwMtB_adHH5EzWk0QuMLAs-I_Etyfa1skTb0bLn0dAmg |
| ABCDEF123B | eyJhbGciOiJIUzUxMiJ9.eyJhdWRpZW5jZSI6ImVtYmVkZGVkIiwiY3JlYXRlZCI6eyJlcG9jaFNlY29uZCI6MTUyNzQwMDUyNCwibmFubyI6NzQxMDAwMDAwfSwiZGV2aWNlSWQiOiJBQkNERUYxMjNCIn0.E2YoUaCE4UlGYrMlCcB1RuVMhfTMrQ95CE9jnFBBqCUiRCOnu9itoVsTBq6M2yMSDa1zb7JxuCFVPckDPvd29Q |
